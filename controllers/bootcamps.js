const path = require('path');
const ErrorResponse = require('../utils/errorResponse');
const asyncHandler =  require('../middleware/async');
const Bootcamp = require('../models/Bootcamp');
const geocoder = require('../utils/geoCoder');

// @desc    GET all bootcamps
// @route   GET /api/v1/bootcamps
// @access  Public
exports.getBootcamps = asyncHandler(async (req, res, next) => {
  res.status(200).json(res.advancedResults);
});

// @desc    GET bootcamps
// @route   GET /api/v1/bootcamps
// @access  Public
exports.getBootcamp = asyncHandler(async (req, res, next) => {
    const bootcamp = await Bootcamp.findById(req.params.id);
    if(!bootcamp) {
      return next(
        new ErrorResponse(`Bootcamp not found with id of ${req.params.id}`, 404)
      );
    }
    res.status(200).json({ success: true, data: bootcamp });
});

// @desc    Create bootcamps
// @route   POST /api/v1/bootcamps
// @access  Private
exports.createBootcamp = asyncHandler(async (req, res, next) => {
    // Add User to req.body
    req.body.user = req.user.id;

    // Check for published Bootcamp
    const publishedBootcamp = await Bootcamp.findOne({ user: req.user.id });

    // If the user is not an admin, they can only and one Bootcamp
    if (publishedBootcamp && req.user.role !== "admin") {
      return next(new ErrorResponse(`User with id ${req.user.id} has already published a bootcamp`, 
      400 ));
    }
    
    const bootcamp = await Bootcamp.create(req.body);

    res.status(201).json({
      success: true, 
      data: bootcamp 
    });
});

// @desc    Update bootcamps
// @route   PUT /api/v1/bootcamps/:id
// @access  Private
exports.updateBootcamp = asyncHandler(async (req, res, next) => {
    let bootcamp = await Bootcamp.findById(req.params.id);
    if(!bootcamp) {
      return next(
        new ErrorResponse(`Bootcamp not found with id of ${req.params.id}`, 404)
      );
    }
    // MAKE SURE USER IS BOOTCAMP OWNER
    if(bootcamp.user.toString() !== req.user.id && req.user.role !== 'admin') {
      return next(
        new ErrorResponse(`User ${req.params.id} is not authorized to update this bootcamp`
        , 401));
    }
    bootcamp = await bootcamp.findByIdAndUpdate(request.params.id, req.body, {
      new: true,
      runValidators: true
    });

    res
      .status(200)
      .json({ success: true, data: bootcamp });
});

// @desc    Delete bootcamps
// @route   DELETE /api/v1/bootcamps/:id
// @access  Private
exports.deleteBootcamp = asyncHandler(async (req, res, next) => {
    const bootcamp = await Bootcamp.findById(req.params.id);
    if(!bootcamp) {
      return next(
        new ErrorResponse(`Bootcamp not found with id of ${req.params.id}`, 404)
      );
    }

    // MAKE SURE USER IS BOOTCAMP OWNER
    if(bootcamp.user.toString() !== req.user.id && req.user.role !== 'admin') {
      return next(
        new ErrorResponse(`User ${req.params.id} is not authorized to delete this bootcamp`
        , 401));
    }

    bootcamp.remove();
    res
      .status(200)
      .json({ success: true, data: `Deleted object with ${req.params.id}` });
});

// @desc    Get bootcamps within a radius
// @route   GET /api/v1/bootcamps/radius/:zipcode/:distance
// @access  Private
exports.getBootcampsInRadius = asyncHandler(async (req, res, next) => {
  const { zipcode, distance } = req.params;
  
  // Get lat/lng from geoCoder
  console.log(zipcode);
  const loc = await geocoder.geocode(zipcode);
  const lat = loc[0].latitude;
  const lng = loc[0].longitude;
  console.log(lat, lng);
  // Calc radius
  // Divide distance by radius of the earth
  // Earth radius 3963 miles

  const radius = distance / 6398;
  const bootcampsWithinARadius = await Bootcamp.find({
    location: {$geoWithin : { $centerSphere:[[lng, lat], radius ] } }
  });

  res.status(200).json({
    success: true,
    count: bootcampsWithinARadius.length,
    data: bootcampsWithinARadius
  })
});

// @desc    Upload Photos for bootcamp
// @route   PUT /api/v1/bootcamps/:id/photo
// @access  Private
exports.bootcampPhotoUpload = asyncHandler(async (req, res, next) => {
  const bootcamp = await Bootcamp.findById(req.params.id);
  if(!bootcamp) {
    return next(
      new ErrorResponse(`Bootcamp not found with id of ${req.params.id}`, 404)
    );
  }

  // MAKE SURE USER IS BOOTCAMP OWNER
  if(bootcamp.user.toString() !== req.user.id && req.user.role !== 'admin') {
    return next(
      new ErrorResponse(`User ${req.params.id} is not authorized to upload photo for this bootcamp`
      , 401));
  }

  if(!req.files) {
    return next(
      new ErrorResponse(`Please upload a file`, 404));
  }
  const file  = req.files.file;

  // Make sure the image is a photo
  if(!file.mimetype.startsWith('image')) {
    return next(new ErrorResponse(`Please upload an image file`, 400));
  }

  // Check filesize
  if(file.size > process.env.MAX_FILE_UPLOAD) {
    return next(new ErrorResponse(`Please upload an image less than 
    ${process.env.MAX_FILE_UPLOAD}`, 400));
  }

  // Create custom filename
  file.name = `photo_${bootcamp._id}${path.parse(file.name).ext}`;
  file.mv(`${process.env.FILE_UPLOAD_PATH}/${file.name}`, async err => {
    if(err) {
      console.error(err);
      return next(
        new ErrorResponse(`Problem with file upload`, 500));
    }

    await Bootcamp.findByIdAndUpdate(req.params.id, { photo: file.name });
    
    res.status(200).json({
      success: true,
      data: file.name
    });
  });
});