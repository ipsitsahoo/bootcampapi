const path = require('path');
const express = require('express');
const dotenv = require('dotenv');
const morgan = require('morgan');
const colors = require('colors');
const fileUpload = require('express-fileupload');
const cookieParser = require('cookie-parser');
const errorHandler = require('./middleware/error');
const connectDB = require('./config/db');
const sanitizer = require('express-mongo-sanitize');
const xss = require('xss-clean');
const helmet = require('helmet');
const expressRateLimit = require('express-rate-limit');
const hpp = require('hpp');
const cors = require('cors');

// Load env vars
dotenv.config({path : './config/config.env'});

// Connect to Database
connectDB();

// Routes
const bootcamps = require('./routes/bootcamps');
const courses = require('./routes/courses');
const auth = require('./routes/auth');
const users = require('./routes/users');
const reviews = require('./routes/reviews');

const app = express();

// Body parser
app.use(express.json());

// Use cookie parser
app.use(cookieParser());

// Dev logging middleware
if(process.env.NODE_ENV === 'development') {
    app.use(morgan('dev'));
}

// File uploading
app.use(fileUpload());

// Mongo Sanitizer
app.use(sanitizer());

// Set security headers
app.use(helmet());

// Prevent XSS/ Cross Scripting attacks
app.use(xss());

// Enable cors
app.use(cors());

// Rate Limiter
const limiter = expressRateLimit({
    windowMs: 10 * 10 * 1000,
    max: 100
});
app.use(limiter);

// Prevent http Param pollution
app.use(hpp);

// Set statis folder
app.use(express.static(path.join(__dirname, 'public')));

// Mount routers
app.use('/api/v1/bootcamps', bootcamps);
app.use('/api/v1/courses', courses);
app.use('/api/v1/auth', auth);
app.use('/api/v1/auth/users', users);
app.use('/api/v1/reviews', reviews);

app.use(errorHandler);

const PORT = process.env.PORT || 5000;

const server = app.listen(PORT,
    console.log(`Server running in ${process.env.NODE_ENV} mode on port ${PORT}`.yellow.bold));


// Handle unhandled promise rejections
process.on('UnhandledRejection', (err, promise) => {
    console.log(`Error: ${err.message}`.red);
    // Close and Exit server
    server.close(() => process.exit(1));
});