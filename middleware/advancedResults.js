const advancedResults = (model, populate) => async (req, res, next) => {
    
    let query;
    // Copy request query
    const reqQuery = {...req.query};

    // Fields to exclude
    const removeFields = ['select', 'sort', 'page', 'limit'];

    // Loop over removeFields and delete them from reqQuery
    removeFields.forEach(param => delete reqQuery[param]);

    // Create query string 
    let queryStr = JSON.stringify(reqQuery);

    // Create query string for #gt, $lte in the query string.
    queryStr = queryStr.replace(/\b(gt|gte|lt|lte|in)\b/g, match => {
        return `$${match}`;
    });

    // Finding resource
    query = model.find(JSON.parse(queryStr));

    // Select Fields
    if(req.query.select) {
        const fields = req.query.select.split(',').join(' ');
        query = query.select(fields);
    }

    // Sort
    if(req.query.sort)
    {
        const sortby = req.query.sort.split(',').join(' ');
        query = query.sort(sortby);
    } else {
        query = query.sort('-createdAt');
    }

    // Pagination
    const page = parseInt(req.query.page, 10) || 1;
    const limit = parseInt(req.query.limit, 10) || 25;
    const startindex = (page - 1) * limit;
    const endindex = page * limit;
    const total = await model.countDocuments();

    console.log(page, limit);
    query = query.skip(startindex).limit(limit);
    if(populate) {
        query = query.populate(populate);
    }

    // Executing query
    const results = await query;

    // Pagination Result
    const pagination = {};

    if(endindex < total) {
        pagination.next = {
            page: page + 1,
            limit
        }
    }
    if(startindex > 0) {
        pagination.prev = {
            page: page - 1,
            limit
        }
    }

    res.advancedResults = {
        success: true,
        count: results.length,
        pagination,
        data: results
    };

    next();
}

module.exports = advancedResults;